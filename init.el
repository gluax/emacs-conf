(server-start)

(setq custom-file "~/.emacs.d/lib/core/autogen.el")
(load-file "~/.emacs.d/lib/core/autogen.el")
(load-file "~/.emacs.d/lib/core/package_managers.el")

(defun load-dir-files (dir)
  (mapc 'load (file-expand-wildcards (concat dir "/*.el"))))

(defun lib-files ()
  (cdddr (directory-files "~/.emacs.d/lib" t)))

(defun init ()
  (mapc 'load-dir-files (lib-files)))

(init)

