;;; web.el --- Web languages setup.

;;; Commentary:
;; Web languages setup.

;;; Code:
(use-package scss-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode)))

(use-package typescript-mode)

(use-package deno-fmt
  :hook (js2-mode . typescript-mode))

;; (use-package tide
;;   :ensure t
;;   :conifg
;;   (add-hook 'before-save-hook 'tide-format-before-save)
;;   (add-hook 'typescript-mode-hook #'setup-tide-mode))

(use-package web-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.js[x]?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
  :custom
  (web-mode-ac-sources-alist
	    '(("css" . (ac-source-css-property))
	      ("html" . (ac-sourc-words-in-buffer ac-source-abbrev))))
  (web-mode-enable-auto-closing t)
  (web-mode-enable-auto-quoting t))

(provide 'web)
;;; web.el ends here
