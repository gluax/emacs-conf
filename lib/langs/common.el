;;; common.el --- Common lang setup features.

;;; Commentary:
;; Package setsup the common language features like auto-completion.

;;; Code:
(use-package company
  :defer t
  :init (add-hook 'after-init-hook 'global-company-mode)
  :config
  ;; Use Company for completion
  (bind-key [remap completion-at-point] #'company-complete company-mode-map)
  :custom
  (company-tooltip-align-annotations t)
  (company-show-numbers t)
  :bind (("C-;" . company-complete-common))
  :diminish company-mode)

(use-package company-org-roam
  :after (company)
  :config
  (push 'company-org-roam company-backends)
  :diminish)

(use-package flycheck
 :config
 (setq ispell-program-name "hunspell")
 (setq ispell-local-dictionary "en_US")
 :init
 (global-flycheck-mode)
 (flyspell-mode)
 :diminish flycheck-mode
 :diminish flyspell-mode)

(use-package lsp-mode)

(provide 'common)
;;; common.el ends here
