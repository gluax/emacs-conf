;;; go.el --- Golang language setup.

;;; Commentary:
;; Golang language setup.

;;; Code:
(use-package go-mode
  :init
  (setq gofmt-command "goimports")
  (defun custom-go-mode-hook ()
    (setq tab-width 2 indent-tabs-mode 1)
    (go-eldoc-setup)
    (local-set-key (kbd "M-.") 'godef-jump)
    (add-hook 'before-save-hook 'gofmt-before-save))
  (add-hook 'go-mode-hook 'custom-go-mode-hook))

(use-package go-eldoc
  :init
  (add-hook 'go-mode-hook 'go-eldoc-setup))

(use-package gotest
  :after go-mode)

;; (use-package go-projectile
;;   :after projectile
;;   :ensure t)

(use-package company-go
  :after company
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'company
    (add-to-list 'company-backends 'company-go)))

(provide 'go)
;;; go.el ends here
