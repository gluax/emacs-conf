;;; minor.el --- Minor file mode configurations.

;;; Commentary:
;; Minor file mode configurations.

;;; Code:
(use-package dockerfile-mode
  :config
  (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode)))

(use-package qml-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.qml$" . qml-mode))
  :custom
  (qml-mode:qml-indent-width 2))

(use-package json-mode)

(use-package fish-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.fish$" . fish-mode)))

(use-package toml-mode)

(use-package yaml-mode)

(provide 'minor)
;;; minor.el ends here
