;;; cl.el --- Common Lisp language setup.

;;; Commentary:
;; Common Lisp language setup.

;;; Code:
(use-package sly)

(if (string-equal system-type "windows-nt")
     (load (expand-file-name "C:/Users/jonat/.roswell/helper.el")))

(provide 'cl)
;;; cl.el ends here
