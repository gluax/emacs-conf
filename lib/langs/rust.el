;;; cl.el --- Common Lisp language setup.

;;; Commentary:
;; Common Lisp language setup.

;;; Code:
(use-package rustic
  :custom
  (if (string-equal system-type "windows-nt")
      (progn
	(message "Windows: setting rust-analyzer")
	(setq lsp-rust-analyzer-server-command '("d:/rust_analyzer/rust-analyzer.exe"))))
  (rustic-lsp-server 'rust-analyzer))

(provide 'rust)
;;; rust.el ends here
