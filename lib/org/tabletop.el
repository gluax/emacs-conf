;;; tabletop.el --- Tabletop org setup features.

;;; Commentary:
;; Package sets up features useful for TTRPGs in org mode.

;;; Code:

(use-package org-d20
  :after (org))

(provide 'tabletop)
;;; tabletop.el ends here
