;;; roam.el --- Enabling and configuring org-roam

;;; Commentary:
;; Package configures: org-roam, org-roam-server, and org-journal.

;;; Code:

(use-package org-roam
  :diminish org-roam-mode
  :config
  (org-roam-db-autosync-mode)
  :init (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/org-roam")
  :bind (("C-c n l" . org-roam-buffer-toggle)
               ("C-c n f" . org-roam-node-find)
               ("C-c n g" . org-roam-graph))
              (("C-c n i" . org-roam-node-insert))
              (("C-c n c" . org-roam-capture)))

(use-package org-roam-bibtex
    :after org-roam
    :hook (org-roam-mode . org-roam-bibtex-mode)
    :bind (:map org-mode-map
		(("C-c n a" . orb-note-actions))))

(use-package websocket
  :after org-roam)

(use-package simple-httpd)

(use-package org-roam-ui
  :straight (org-roam-ui :host github :repo "org-roam/org-roam-ui" :branch "main" :files ("*.el" "out"))
  :after (org-roam websocket simple-httpd)
  :bind
  ("<f2>" . org-roam-ui-mode)
  ("<f3>" . org-toggle-inline-images))

(use-package org-download
  :after org
  :bind
  (:map org-mode-map
        (("s-Y" . org-download-screenshot)
         ("s-y" . org-download-yank))))

(provide 'roam)
;;; roam.el ends here
