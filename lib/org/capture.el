;;; capture.el --- Custom org templates.

;;; Commentary:
;; Package creates custom templates.
;; Game ideas, Notes, Projectile Projects and Story Ideas.

;;; Code:
(defvar org-capture-templates)
(setq org-capture-templates
      '(
        ("g" "Game Idea" entry (file+headline "~/docs/orgfiles/games.org" "Game Ideas:")
         "* TODO %?\n:PROPERTIES:\n\n:END:\n:DEADLINE: %^T \n %i\n")
        ("n" "Notes" entry (file+headline "~/docs/orgfiles/notes.org" "Notes:")
         "* %u %?" :prepend t)
        ("t" "Projectile Projects" entry (file "~/docs/orgfiles/projects.org")
         "* %^{Project Path}")
        ("s" "Story Idea" entry (file+headline "~/docs/orgfiles/stories.org" "Story Ideas:")
         "* TODO %?\n:PROPERTIES:\n\n:END:\n:DEADLINE: %^T \n %i\n")
        ))

(defadvice org-capture-finalize
    (after delete-capture-frame activate)
  "Advise capture-finalize to close the frame."
  (if (equal "capture" (frame-parameter nil 'name))
      (delete-frame)))

(defadvice org-capture-destroy
    (after delete-capture-frame activate)
  "Advise capture-destroy to close the frame."
  (if (equal "capture" (frame-parameter nil 'name))
      (delete-frame)))

(use-package noflet)

(defun make-capture-frame ()
  "Create a new frame and run 'org-capture'."
  (interactive)
  (make-frame'((name . "capture")))
  (select-frame-by-name "capture")
  (delete-other-windows)
  (noflet ((switch-to-buffer-other-window (buf) (switch-to-buffer buf)))
    (org-capture)))

(provide 'capture)
;;; capture.el ends here
