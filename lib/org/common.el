;;; common.el --- Common org setup features.

;;; Commentary:
;; Package sets up the basic org mode features.

;;; Code:
(use-package org
  :straight nil
  :hook (org-mode . org-indent-mode)
  :bind (
	 ("C-c a" . 'org-agenda)
	 ("C-c l" . 'org-store-link)
	 ("C-c k" . 'org-capture)
         )
  :custom
  (org-directory "~/org")
  (org-export-html-postamble nil)
  (org-startup-indented t)
  (org-startup-folded (quote overview))
  ;; (org-agenda-files '("~/org/agenda/"))
  (org-latex-listings 'minted)
  (org-latex-minted-options '(("frame" "lines") ("linenos=true")))
  (org-latex-packages-alist '(("" "minted")))
  (org-latex-pdf-process
   '("xelatex -8bit --enable-8bit-chars -shell-escape -interaction nonstopmode -output-directory %o %f"
     "xelatex -8bit --enable-8bit-chars -shell-escape -interaction nonstopmode -output-directory %o %f")

  ;; :config
   (require 'org-latex)
   (require 'ox-md nil t)
   (require 'ox-beamer)
  (with-eval-after-load 'ox-latex
    (defvar org-latex-classes)
    (add-to-list 'org-latex-classes '("letter" "\\documentclass{letter}" ("\\begin{letter}"
									  "\\end{letter}"
									  "\\begin{letter}"
									  "\\end{letter}"))))
  (with-eval-after-load 'ox-md)
  ))

(use-package org-bullets
  :after (org)
  :init (add-hook 'org-mode-hook (lambda () (org-bullets-mode t))))

(use-package org-transclusion
  :straight (org-transclusion :host github :repo "nobiot/org-transclusion" :branch "main")
  :after (org)
  :custom
  (org-transclusion-include-first-section t)
  :config
  (global-set-key "" 'org-transclusion-mode))

(use-package company-org-block
  :after (company-mode)
  :custom
  (company-org-block-edit-style 'auto) ;; 'auto, 'prompt, or 'inline
  :hook ((org-mode . (lambda ()
                       (setq-local company-backends '(company-org-block))
                       (company-mode +1)))))

(use-package ox-reveal)

(provide 'common)
;;; common.el ends here
