;;; writing.el --- Additional features for writing in Emacs.

;;; Commentary:
;; Package adds additional features for writing in Emacs.

;;; Code:
(use-package dictionary
  :bind ("<f8>" . 'dictionary-search))

(use-package synosaurus
  :bind ("<f9>" . 'synosaurus-choose-and-replace))

(use-package visual-fill-column
  :init
  (add-hook 'visual-line-mode-hook 'visual-fill-column-mode))

;; (use-package writeroom-mode
;;   :bind ("<f4>" . 'whiteroom-mode))

(use-package fountain-mode
  :init
  (add-to-list 'auto-mode-alist '("\\.fountain\\'" . fountain-mode)))

(use-package ispell
  :custom
  (ispell-program-name "hunspell")
  :bind ("<f10>" . ispell-buffer))

(provide 'writing)
;;; writing.el ends here
