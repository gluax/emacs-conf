;;; autogen.el --- The autogen content from Emacs.

;;; Commentary:
;; The autogen content from Emacs.
;; Also configures straight and use package.

;;; Code:
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-show-quick-access t nil nil "Customized with use-package company")
 '(org-agenda-files nil nil nil "Customized with use-package org")
 '(package-selected-packages
   '(use-package-el-get el-get org-transclusion cmake-mode qml-mode bazel-mode org-roam-server org-roam-protocol org-roam lsp-mode dockerfile-mode typescript-mode web-mode deno-fmt typescript scss-mode toml-mode docker-compose-mode docker-mode synosaurus dictionary org-ac org-bullets org-plus-contrib noflet rustic company-go gotest go-eldoc go-mode flycheck company sly color-theme git-timemachine magit undo-tree easy-kill fzf wgrep pcre2el which-key try hungry-delete counsel multiple-cursors expand-region ace-window use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 3.0)))))

(provide 'autogen)
;;; autogen.el ends here
