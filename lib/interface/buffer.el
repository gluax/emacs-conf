;;; buffer.el --- Better buffer list interface.

;;; Commentary:
;; Better buffer list interface and movement.

;;; Code:
(defalias 'list-buffers 'ibuffer)

(use-package ace-window
  :init
  (progn
    (global-set-key[remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))
    ))

(provide 'buffer)
;;; buffer.el ends here
