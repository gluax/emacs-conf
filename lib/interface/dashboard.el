;;; dashboard.el --- Startup Dashboard.

;;; Commentary:
;; Custom startup dashboard.

;;; Code:
(use-package dashboard
  :custom
  (dashboard-set-heading-conts t)
  (dashboard-set-file-icons t)
  (dashboard-center-content t)
  (dashboard-set-init-info t)
  (dashboard-set-footer nil)
  :config (dashboard-setup-startup-hook))
  
(provide 'dashboard)
;;; dashboard.el ends here
