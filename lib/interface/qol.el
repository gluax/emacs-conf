;;; qol.el --- Quality of life package setups.

;;; Commentary:
;; Inits packages that improves the base functions provided by Emacs.

;;; Code:
(use-package hungry-delete
  :config
  (global-hungry-delete-mode))

(use-package try)

(use-package which-key
  :config (which-key-mode))

(use-package pcre2el
  :config
  (pcre-mode))

(use-package wgrep)

(use-package fzf)

(use-package easy-kill
  :config
  (global-set-key [remap kill-ring-save] #'easy-kill)
  (global-set-key [remap mark-sexp] #'easy-mark))

(use-package undo-tree
  :init (global-undo-tree-mode))

(use-package magit
  :bind ("C-x g" . 'magit-status))

(use-package forge)

(use-package git-timemachine)

(use-package rainbow-delimiters
  :config
  (progn
    (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)))

(provide 'qol)
;;; qol.el ends here
