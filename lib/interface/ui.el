;;; ui.el --- Changes to the Emacs default UI settings.

;;; Commentary:
;; Package changes to the Emacs default UI settings.

;;; Code:
(use-package emacs
  :hook (after-save . (lambda ()
		      (if (string-match-p (regexp-quote ".emacs.d") default-directory)
			  (byte-compile-file "~/.emacs.d/init.el")
			  (byte-recompile-directory (expand-file-name "~/.emacs.d/lib") 0))))
  :init
  (setq inhibit-splash-screent t)
  (setq save-interprogram-paste-before-kill t)
  (add-to-list 'default-frame-alist '(font . "OpenDyslexic 27"))
  (fset 'yes-or-no-p 'y-or-n-p)
  :config
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (toggle-scroll-bar -1)
  (global-linum-mode t)
  (electric-pair-mode t)
  (global-auto-revert-mode t)
  (global-set-key (kbd "<f5>") 'revert-buffer))

(use-package solarized-theme
  :config
  (load-theme 'solarized-dark t))

(use-package all-the-icons-ivy
  :init (add-hook 'after-init-hook 'all-the-icons-ivy-setup))

(provide 'ui)
;;; ui.el ends here
