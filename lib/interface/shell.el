;;; shell.el --- Quality of life package setups.

;;; Commentary:
;; Inits packages that improves the base functions provided by Emacs.

;;; Code:
(use-package esh-autosuggest
  :after (eshell)
  :hook (eshell-mode . esh-autosuggest-mode))

(use-package eshell-fixed-prompt
  :after (eshell)
  :hook (eshell-mode . eshell-fixed-prompt-mode))

(use-package eshell-syntax-highlighting
  :after (esh-mode)
  :hook (eshell-mode . eshell-syntax-highlighting-mode))

(use-package eshell-up
  :init (setq eshell-up-ignore-case nil))

(provide 'shell)
;;; shell.el ends here
