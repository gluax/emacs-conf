;;; keybind.el --- Keybinds for all the things.

;;; Commentary:
;; Package initializes many common keybindings.

;;; Code:
;; Remove sending Emacs to foreground.
(global-unset-key (kbd "C-z"))

(use-package expand-region
  :bind ("C-=" . 'er/expand-region))

(use-package multiple-cursors
  :bind (
	  ("C->" . 'mc/mark-next-like-this)
	  ( "C-<" . 'mc/mark-previous-like-this)
	  ( "C-;" . 'mc/mark-all-like-this)
	  ) )

(use-package avy
  :config
  (global-set-key (kbd "C-:") 'avy-goto-char)
  (global-set-key (kbd "C-'") 'avy-goto-char-2))

(use-package counsel
  :after swiper
  :config (counsel-mode)
  :bind (("C-s" . swiper)
	 ("C-r" . swiper)
	 ("C-c C-r" . ivy-resume)
	 ("M-x" . counsel-M-x)
	 ("C-x C-f" . counsel-find-file)
	 ("M-y" . counsel-yank-pop)))

(use-package ivy
  :defer 0.1
  :diminish
  :bind (("C-x b" . ivy-switch-buffer))
  :config (ivy-mode)
  :custom
  (ivy-use-virtual-buffers t)
  (ivy-display-style 'fancy))

(use-package ivy-rich
  :after ivy
  :custom
  (ivy-virtual-abbreviate 'full
                          ivy-rich-switch-buffer-align-virtual-buffer t
                          ivy-rich-path-style 'abbrev))

(use-package swiper
  :after ivy)

(provide 'keybind)
;;; keybind.el ends here
