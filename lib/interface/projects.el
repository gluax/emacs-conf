;;; projects.el --- Sets up projectile.

;;; Commentary:
;; Projectile setup for package management.

;;; Code:
(use-package rg
  :init (rg-enable-menu))

(use-package projectile
  :init (setq projectile-project-search-path '("~/projects/" "~/work/"))
  :config (projectile-mode +1)
  :bind (:map projectile-mode-map
	      ("s-p" . projectile-command-map)
	      ("C-c p" . projectile-command-map)))

(provide 'projects)
;;; projects.el ends here
